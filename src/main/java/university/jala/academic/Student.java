package university.jala.academic;

import java.util.Comparator;

/**
 * Student.
 */
public class Student {
    private String name;
    private int grade;

    public Student(String name, int grade) {
        this.name = name;
        this.grade = grade;
    }

    public int getGrade() {
        return grade;
    }

    public static Comparator<Student> byGrade() {
        return Comparator.comparingInt(Student::getGrade);
    }

    public String toString() {
        return "{" + name + "," + grade + "}";
    }
}