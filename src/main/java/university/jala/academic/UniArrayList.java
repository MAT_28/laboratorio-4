package university.jala.academic;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class UniArrayList<E> implements List<E>, Sortable<E>, Unique<E> {
    private Object[] elements;
    private int size;

    public UniArrayList() {
        this.elements = new Object[0];
        this.size = 0;
    }

    public UniArrayList(E[] ints) {
        this.elements = new Object[ints.length];

        for (int i = 0; i < ints.length; i++) {

            elements[i] = ints[i];
        }

        this.size = ints.length;
    }

    @Override
    public int size() {

        return size;
    }

    @Override
    public boolean isEmpty() {

        return false;
    }

    @Override
    public boolean contains(Object o) {

        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            private int currentIndex = 0;

            @Override
            public boolean hasNext() {

                return currentIndex < size;
            }

            @Override
            public E next() {

                return (E) elements[currentIndex++];
            }
        };
    }

    @Override
    public Object[] toArray() {

        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {

        return null;
    }

    @Override
    public boolean add(E e) {

        ensureCapacity(size + 1);
        elements[size++] = e;
        return true;
    }

    @Override
    public boolean remove(Object o) {

        for (int i = 0; i < size; i++) {

            if (o.equals(elements[i])) {

                remove(i);
            }
        }
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {

        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {

        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {

        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {

        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {

        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public E get(int index) {

        return (E) elements[index];
    }

    @Override
    public E set(int index, E element) {

        return null;
    }

    @Override
    public void add(int index, E element) {
        ensureCapacity(size + 1);

        for (int i = size; i > index; i--) {

            elements[i] = elements[i - 1];
        }

         elements[index] = element;

        size++;
    }

    @Override
    public E remove(int index) {

        E removedElement = get(index);

        for (int j = index; j < size - 1; j++) {

            elements[j] = elements[j + 1];
        }

        elements[size - 1] = null;
        size--;

        return removedElement;
    }

    @Override
    public int indexOf(Object o) {

        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {

        return 0;
    }

    @Override
    public ListIterator<E> listIterator() {

        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {

        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {

        return null;
    }

    @Override
    public void sort() {
    }

    @Override
    public void sortBy(Comparator<E> comparator) {

        for (int i = 0; i < size - 1; i++) {

            for (int j = 0; j < size - i - 1; j++) {

                if (comparator.compare((E) elements[j], (E) elements[j + 1]) > 0) {

                    E temp = (E) elements[j];
                    elements[j] = elements[j + 1];
                    elements[j + 1] = temp;
                }
            }
        }
    }

    @Override
    public void unique() {
        
    }

    public String toString(){
        StringBuilder result = new StringBuilder("[");

        for (int i = 0; i < size; i++) {

            result.append(elements[i]);

            if (i < size - 1) {

                result.append(", ");
            }
        }

        result.append("]");
        return result.toString();
    }

    private void ensureCapacity(int minCapacity) {

        if (minCapacity > elements.length) {

            int newCapacity = Math.max(elements.length * 2, minCapacity);
            resize(newCapacity);
        }
    }

    private void resize(int newCapacity) {

        Object[] newElements = new Object[newCapacity];

        for (int i = 0; i < size; i++) {
            newElements[i] = elements[i];
        }

        elements = newElements;
    }
}
